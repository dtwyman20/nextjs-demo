/**
 * @jest-environment jsdom
 */

import React from 'react'
import { render, screen } from '@testing-library/react'
import Home from '../pages/index'

describe('Home', () => {
  it('renders a heading', () => {
    render(<Home />);
    const myText = screen.getByText(/My App/i);

    expect(myText).toBeInTheDocument()
  });

  it("should have add one button", () => {
    render(<Home />);
    const myButton = screen.getByTestId("add-one-button");

    expect(myButton).toBeInTheDocument();
  });

  it("should have subtract one button", () => {
    render(<Home />);
    const myButton = screen.getByTestId("subtract-one-button");

    expect(myButton).toBeInTheDocument();
  });

  it("should have reset button", () => {
    render(<Home />);
    const myButton = screen.getByTestId("reset-button");

    expect(myButton).toBeInTheDocument();
  });
});


